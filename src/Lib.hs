module Lib
    ( Instrument, defaultInstrument
    , MusicState(..)
    , Duration(..), whole, half, fourth, eighth, sixteenth
    , (.+), (.-), h, w
    , middleC, octave
    , nC, nCs, nDb, nD, nDs, nEb, nE, nF, nFs, nGb, nG, nGs, bAb, nA, nAs, nBb, nB
    , Transposable(..), Interval(..)
    , Note(..), Rest(..), Playable(..), HasName(..)
    , noteFromParts, noteToParts, normalize
    , Seq(..)
    , Chord, notes
    , majChord, minChord, augChord, dimChord
    , maj7Chord, dom7Chord, min7Chord, minmaj7Chord, halfdim7Chord, dim7Chord, augmaj7Chord, aug7Chord
    , invert
    , Key, majKey, minKey, nthNote, nthChord, steps, chords
    , Music(..), runMusic
    ) where

import GHC.Float
import System.MIDI hiding (getName)
import qualified System.MIDI (getName)
import Control.Concurrent
import Control.Monad.State

data Instrument = Instrument { channel :: Int, con :: Connection }

data MusicState = MusicState { instrument :: Instrument
                             , bpm :: Float
                             , timeSig :: (Int, Int)
                             , defaultNoteDuration :: Duration }

type Music a = StateT MusicState IO a

getBpm :: Music Float
getBpm = do
  st <- get
  return $ bpm st

noteOn :: Int -> Int -> Music ()
noteOn val vel = do
  st <- get
  let i = instrument st
  liftIO $ send (con i) $ MidiMessage (channel i) $ NoteOn val vel

noteOff :: Int -> Int -> Music ()
noteOff val vel = do
  st <- get
  let i = instrument st
  liftIO $ send (con i) $ MidiMessage (channel i) $ NoteOff val vel

class Playable a where
  playFor :: a -> Duration -> Music ()
  play :: a -> Music ()
  play x = do
    st <- get
    x `playFor` (defaultNoteDuration st)

class HasName a where
  getName :: a -> String

data Duration = Subdiv Int Int

whole, half, fourth, eighth, sixteenth :: Duration
whole = Subdiv 1 1
half = Subdiv 1 2
fourth = Subdiv 1 4
eighth = Subdiv 1 8
sixteenth = Subdiv 1 16

toMicroseconds :: Duration -> Music Int
-- toMicroseconds (Seconds s) = pure $ truncateFloat $ 1000000 * s
toMicroseconds (Subdiv n subDiv) = do
  st <- get
  let (_, wholeDiv) = timeSig st
  return $ truncateFloat $
    (int2Float $ n * 60000000) / ((bpm st) * ((int2Float wholeDiv) / (int2Float subDiv)))

newtype Note = Note { value :: Int } deriving (Eq, Ord)
newtype Rest = Rest { duration :: Duration }

class Transposable a where
  infixl 6 .+
  infixl 6 .-
  (.+) :: a -> Interval -> a
  (.-) :: a -> Interval -> a
  (.-) x i = x .+ (-i)

instance Transposable Note where
  (.+) (Note v) i = Note $ v + (intervalHalfSteps i)

noteToParts :: Note -> (Int, Int)
noteToParts (Note v) = (v `mod` 12, (v `div` 12) - 1)

noteFromParts :: (Int, Int) -> Note
noteFromParts (s, o) = Note (s + o * 12)

normalize :: Note -> Note
normalize (Note v) = (Note (v `mod` 12)) .+ 4 * octave

nC, nCs, nDb, nD, nDs, nEb, nE, nF, nFs, nGb, nG, nGs, bAb, nA, nAs, nBb, nB :: Note
nC = Note 60
nCs = Note 61
nDb = Note 61
nD = Note 62
nDs = Note 63
nEb = Note 63
nE = Note 64
nF = Note 65
nFs = Note 66
nGb = Note 66
nG = Note 67
nGs = Note 68
bAb = Note 68
nA = Note 69
nAs = Note 70
nBb = Note 70
nB = Note 71

middleC :: Note
middleC = nC

newtype Interval = Interval { intervalHalfSteps :: Int }
    deriving (Show, Num, Eq, Ord)

octave, w, h :: Interval
octave = 12
w = 2
h = 1

instance HasName Note where
  getName n = (stepNames !! s) ++ (show o)
    where
        stepNames = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]
        (s, o) = noteToParts n

instance Playable Note where
  playFor n d = do
    noteOn (value n) 65
    dur <- toMicroseconds d
    liftIO $ threadDelay dur
    noteOff (value n) 65

instance Playable Rest where
  playFor r _ = do
    dur <- toMicroseconds (duration r)
    liftIO $ threadDelay dur

data Seq = Seq [[Note]]

instance Playable Seq where
  playFor (Seq ps) d = helper ps
    where
      helper [] = return ()
      helper (p:ps) = do
        mapM_ (\n -> noteOn (value n) 65) p
        dur <- toMicroseconds d
        liftIO $ threadDelay dur
        mapM_ (\n -> noteOff (value n) 65) p
        helper ps

data Chord = Chord { chordName :: String
                   , notes :: [Note] }

maj3rd, min3rd, per5th, dim5th, aug5th :: Interval
maj3rd = 2 * w
min3rd = maj3rd - h
per5th = 3 * w + h
dim5th = per5th - h
aug5th = per5th + h

maj7th, min7th, dim7th :: Interval
maj7th = octave - h
min7th = maj7th - h
dim7th = min7th - h

mkChord :: String -> [Interval] -> Note -> Chord
mkChord prefix steps root =
  Chord prefix $ root : (map (root .+) steps)

majChord, minChord, dimChord, augChord :: Note -> Chord
majChord = mkChord "maj" [maj3rd, per5th]
minChord = mkChord "min" [min3rd, per5th]
dimChord = mkChord "dim" [min3rd, dim5th]
augChord = mkChord "aug" [maj3rd, aug5th]

maj7Chord, dom7Chord, min7Chord, minmaj7Chord, halfdim7Chord, dim7Chord, augmaj7Chord, aug7Chord :: Note -> Chord
maj7Chord     = mkChord "maj7"    [maj3rd, per5th, maj7th]
dom7Chord     = mkChord ""        [maj3rd, per5th, min7th]
min7Chord     = mkChord "min7"    [min3rd, per5th, min7th]
minmaj7Chord  = mkChord "minmaj7" [min3rd, per5th, maj7th]
halfdim7Chord = mkChord "halfdim7"[min3rd, dim5th, min7th]
dim7Chord     = mkChord "dim7"    [min3rd, dim5th, dim7th]
augmaj7Chord  = mkChord "augmaj7" [maj3rd, aug5th, maj7th]
aug7Chord     = mkChord "aug7"    [maj3rd, aug5th, min7th]

invert :: Int -> Chord -> Chord
invert n (Chord p ss) = Chord p $
  if n < 0 then reverse $ invertHelper (-octave) (-n) $ reverse ss
  else invertHelper octave n ss
  where
    invertHelper _ 0 ss = ss
    invertHelper k n (s:ss) = invertHelper k (n-1) $ ss ++ [s .+ k]
invert _ _ = error "Invalid chord inversion"

instance HasName Chord where
  getName c = (chordName c) ++ (getName $ head $ notes c)

instance Playable Chord where
  playFor (Chord _ notes) d = do
    mapM_ (\n -> noteOn (value n) 65) notes
    dur <- toMicroseconds d
    liftIO $ threadDelay dur
    mapM_ (\n -> noteOff (value n) 65) notes

data Key = Key { keyName :: String
               , chords :: [Chord]
               , steps :: [Note] }

instance HasName Key where
  getName = keyName

instance Transposable Chord where
  (.+) c i = Chord (chordName c) (map (.+ i) $ notes c)

inKey :: Transposable a => (Int -> a) -> Int -> a
inKey f n | n < 0 = (inKey f $ n + 7) .- octave
inKey f n | n >= 7 = (inKey f $ n - 7) .+ octave
inKey f n = f n

mkKey :: String -> [Interval] -> [Note -> Chord] -> Note -> Key
mkKey prefix steps chords root =
  Key (prefix ++ (getName root)) (map (\(f, x) -> f x) $ zip chords notes) $ notes
  where
    notes = scanl (.+) root steps

majKey :: Note -> Key
majKey =
  mkKey "maj" [w, w, h, w, w, w] [ majChord, minChord, minChord, majChord
                                 , majChord, minChord, dimChord ]

minKey :: Note -> Key
minKey =
  mkKey "min" [w, h, w, w, h, w] [ minChord, dimChord, majChord, minChord
                                 , minChord, majChord, majChord ]

nthNote :: Key -> Int -> Note
nthNote s = inKey (\n -> (steps s) !! n)

nthChord :: Key -> Int -> Chord
nthChord s = inKey (\n -> (chords s) !! n)

defaultInstrument :: Int -> IO (Maybe Instrument)
defaultInstrument ch = do
  devs <- enumerateDestinations
  case devs of
    (d:_) -> do
      con <- openDestination d
      start con
      return $ Just $ Instrument ch con
    _ -> return Nothing


runMusic :: Instrument -> Float -> Music a -> IO a
runMusic i bpm m = evalStateT m (MusicState i bpm (4, 4) fourth)
