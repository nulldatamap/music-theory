module Parser
  ( NoteLetter(..), Accidental(..), Note(..), ValuedNote(..), Chord(..)
  , NoteGroup(..), NoteSequence(..)
  , Chord(..), ChordQuality(..)
  , Key(..), KeyKind(..)
  , Command(..)
  , pSeq, pChords, pCommandOr
  ) where

import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Data.Text as T
import Data.Text (Text)
import Data.List (intercalate)
import Data.Maybe (fromMaybe)
import Data.Void

import Lib ()

type Parser = Parsec Void Text

data NoteLetter = A | B | C | D | E | F | G deriving Show
data Accidental = Sharp | Flat | Natural
data Note = Note NoteLetter (Maybe Accidental)
data ValuedNote = ValuedNote Note (Maybe Int)
data NoteGroup = NoteGroup [ValuedNote]
data NoteSequence = NoteSequence [NoteGroup]

data ChordQuality = MajorTriad | MinorTriad
                  | HalfDiminishedTriad | DiminishedTriad | AugmentedTriad
                  | DominantSeventh | MajorSeventh | MinorMajorSeventh | MinorSeventh
                  | AugmentedMajorSeventh | AugmentedSeventh | HalfDiminishedSeventh
                  | DiminishedSeventh
                  | Suspended2 | Suspended4
data Chord = Chord Note ChordQuality (Maybe Note)

data KeyKind = MajorKey | MinorKey

data Key = Key NoteLetter KeyKind

data Command = CBpm Float
             | CSig Int Int
             | CKey Key
             | CNotes
             | CChords

instance Show Accidental where
  show Sharp = "♯"
  show Flat = "♭"
  show Natural = "♮"

instance Show Note where
  show (Note n mA) = (show n) ++ (fromMaybe "" $ fmap show mA)

instance Show ValuedNote where
  show (ValuedNote n mO) = (show n) ++ (fromMaybe "" $ fmap show mO)

instance Show NoteGroup where
  show (NoteGroup ns) = intercalate ";" $ map show ns

instance Show NoteSequence where
  show (NoteSequence ns) = intercalate " " $ map show ns

instance Show Chord where
  show (Chord root qual mBass) = (show root) ++ showQual ++ showBass
    where
      showBass =
        case mBass of
          Nothing -> ""
          Just n -> '/' : (show n)
      showQual =
        case qual of
          MajorTriad -> ""
          MinorTriad -> "m"
          DiminishedTriad -> "o"
          AugmentedTriad -> "+"
          HalfDiminishedTriad -> "ø"
          DominantSeventh -> "7"
          MajorSeventh -> "M7"
          MinorMajorSeventh -> "mM7"
          MinorSeventh -> "m7"
          AugmentedMajorSeventh -> "+M7"
          AugmentedSeventh -> "+7"
          HalfDiminishedSeventh -> "ø7"
          DiminishedSeventh -> "o7"
          Suspended2 -> "sus2"
          Suspended4 -> "sus"

noteLetterFromSymbol :: Char -> NoteLetter
noteLetterFromSymbol c =
  case c of
    'A' -> A
    'B' -> B
    'C' -> C
    'D' -> D
    'E' -> E
    'F' -> F
    'G' -> G
    _   -> error $ "Invalid note kind: " ++ [c]

accidentalFromSymbol :: Char -> Accidental
accidentalFromSymbol c =
  case c of
    '#' -> Sharp
    '♯' -> Sharp
    'b' -> Flat
    '♭' -> Flat
    '!' -> Natural
    '♮' -> Natural
    _   -> error $ "Invalid accidental kind: " ++ [c]

sp :: Parser ()
sp = L.space space1 empty empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sp

symbol = L.symbol sp

parens :: Parser a -> Parser a
parens = between (L.symbol sp "(") (L.symbol sp ")")

semi :: Parser ()
semi = () <$ L.symbol sp ";"

pNoteLetter :: Parser NoteLetter
pNoteLetter =
  noteLetterFromSymbol <$> (oneOf ['A'..'G']) <?> "note letter"

pAccidental :: Parser Accidental
pAccidental =
  accidentalFromSymbol <$> (oneOf ['#', '♯', 'b', '♭', '!', '♮'] <?> "accidental")

pNote :: Parser Note
pNote = Note <$> pNoteLetter <*> (optional pAccidental)
     <?> "note"

pValuedNote :: Parser ValuedNote
pValuedNote = ValuedNote <$> pNote <*> (optional L.decimal)
           <?> "valued note"

pGroup :: Parser NoteGroup
pGroup =  parens pGroup
      <|> (NoteGroup <$> sepBy1 pValuedNote semi)
      <?> "note group"

pSeq :: Parser NoteSequence
pSeq = NoteSequence <$> sepBy1 pGroup sp

pChord :: Parser Chord
pChord = lexeme $ do
  root <- pNote
  qual <- (fromMaybe MajorTriad) <$> (optional $
             (  (try $ AugmentedMajorSeventh <$ (aug >> maj >> "7"))
            <|> (try $ MinorMajorSeventh <$ (min >> maj >> "7"))
            <|> (try $ MajorSeventh <$ (maj >> "7"))
            <|> (try $ MinorSeventh <$ (min >> "7"))
            <|> (try $ AugmentedSeventh <$ (aug >> "7"))
            <|> (try $ DiminishedSeventh <$ (dim >> string "7"))
            <|> (try $ HalfDiminishedSeventh<$ string "ø7")
            <|> (try $ DominantSeventh <$ char '7')
            <|> (try $ string "sus" >>
                  (Suspended2 <$ char '2') <|> (Suspended4 <$ char '4') <|> (pure Suspended4))
            <|> (MajorTriad <$ maj)
            <|> (MinorTriad <$ min)
            <|> (AugmentedTriad <$ aug)
            <|> (DiminishedTriad <$ dim)
            <|> (HalfDiminishedTriad <$ halfdim)))
  bass <- optional $ char '/' >> pNote
  return $ Chord root qual bass
  where
    maj = (string "M") <|> (try $ string "maj")
    min = (try $ string "min") <|> (string "m") <|> (string "-")
    dim = (string "o") <|> (string "dim")
    aug = (string "+" <|> (string "aug"))
    halfdim = char 'ø'

pChords :: Parser [Chord]
pChords = many $ pChord

pKeyKind :: Parser KeyKind
pKeyKind =  (MajorKey <$ ((try $ string "major") <|> (try $ "maj") <|> (try $ string "M")))
        <|> (MinorKey <$ ((try $ string "minor") <|> (try $ "min") <|> (try $ string "m")))

-- Fuck roman numerals
-- pChordNum :: Parser IntV
-- pChordNum =  ((1 <$ (try $ string "I"))
--          <|> (2 <$ (try $ string "II"))
--          <|> (3 <$ (try $ string "III"))
--          <|> (4 <$ (try $ string "IV"))
--          <|> (5 <$ (try $ string "V"))
--          <|> (6 <$ (try $ string "VI"))
--          <|> (7 <$ (try $ string "VII")))



pCommand :: Parser Command
pCommand =  (CBpm <$> (symbol ":bpm" >> (L.decimal <|> L.float)))
        <|> (CNotes  <$ symbol ":notes")
        <|> (CChords <$ symbol ":chords")
        <|> (CKey <$> (symbol ":key" >> (Key <$> (lexeme pNoteLetter) <*> (lexeme pKeyKind))))

pCommandOr :: Parser a -> Parser (Either Command a)
pCommandOr p = (Left <$> pCommand) <|> (Right <$> p)
