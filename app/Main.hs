module Main (main) where

import Control.Monad.IO.Class
import System.Exit
import qualified Data.Text.IO as TIO
import System.IO
import Data.Maybe (fromMaybe)
import Data.List (intercalate, elemIndex)
import Control.Monad.State
import Debug.Trace (trace)

import Text.Megaparsec (parse, eof)
import Text.Megaparsec.Error (errorBundlePretty)

import Lib
import qualified Parser as P

data ReplMode = NoteMode | ChordMode | KeyMode

data ReplState = ReplState { replMode :: ReplMode }

song1 :: Music ()
song1 = do
  let key = minKey (nB .- octave)
  -- Riff A
  mapM_ play $ (replicate (4 * 4) $ nthChord key 3)
  mapM_ play $ (take (4 * 4) $ cycle $ map (nthNote key) [5, 4, 2, 4])
  mapM_ play $ (replicate (4 * 4) $ nthChord key 3)
  mapM_ play $ (take (4 * 4) $ cycle $ map (nthNote key) [5, 4, 2, 4])
  -- Riff B
  mapM_ play $ (replicate (4 * 4) $ nthChord (majKey (nD .- octave)) 1)
  mapM_ play $ (replicate (2 * 4) $ (nthChord key 4) .- (2 * octave))
  mapM_ play $ (replicate (1 * 4) $ (nthChord key 5) .- (2 * octave))
  mapM_ play $ (replicate (1 * 4) $ (nthChord key 4) .- (2 * octave))
  mapM_ play $ (replicate (4 * 4) $ nthChord (majKey (nD .- octave)) 1)
  mapM_ play $ (replicate (2 * 4) $ (nthChord key 4) .- (2 * octave))
  mapM_ play $ (replicate (1 * 4) $ (nthChord key 5) .- (2 * octave))
  mapM_ play $ (replicate (1 * 4) $ (nthChord key 4) .- (2 * octave))
  -- Riff C
  mapM_ play $ (replicate (1 * 4) $ nthChord key 3)

  play $ Rest half


song3 :: Music ()
song3 = do
  let key = minKey (nD .- octave)
  liftIO $ putStrLn $ show $ map getName $ steps key
  -- Riff A
  -- mapM_ play $

convertValuedNote :: P.ValuedNote -> Note
convertValuedNote (P.ValuedNote (P.Note n mA) mO) = Note value
  where
    letterValue =
      case n of
        P.C -> 0
        P.D -> 2
        P.E -> 4
        P.F -> 5
        P.G -> 7
        P.A -> 9
        P.B -> 11
    accidentalValue =
      case fromMaybe P.Natural mA of
        P.Natural -> 0
        P.Sharp   -> 1
        P.Flat    -> (-1)
    octaveValue = 12 * (fromMaybe 4 mO)
    value = letterValue + accidentalValue + octaveValue

convertSeq :: P.NoteSequence -> Seq
convertSeq (P.NoteSequence gs) =
  Seq $ map (\(P.NoteGroup ns) -> map convertValuedNote ns) gs

-- data ChordQuality = MajorTriad | MinorTriad
--                   | HalfDiminishedTriad | DiminishedTriad | AugmentedTriad
--                   | DominantSeventh | MajorSeventh | MinorMajorSeventh | MinorSeventh
--                   | AugmentedMajorSeventh | AugmentedSeventh | HalfDiminishedSeventh
--                   | DiminishedSeventh
-- data SuspendedKind = Sus2 | Sus4
-- data Chord = NormalChord Note ChordQuality (Maybe Note)
--            | SuspendedChord Note SuspendedKind

dbg m x = trace (m ++ (show x)) x

convertChord :: P.Chord -> Either String Chord
convertChord (P.Chord root qual mBass) =
  case mBass of
    Nothing -> Right baseChord
    Just pBassNote ->
      let bassNote = convertValuedNote $ P.ValuedNote pBassNote Nothing
      in case bassNote `elemIndex` (map normalize $ notes baseChord) of
           Nothing -> Left $ "Bass note not in chord!"
           Just i -> Right $ invert (-i) baseChord
  where
    baseChord = chordF (convertValuedNote $ P.ValuedNote root Nothing)
    chordF =
      case qual of
        P.MajorTriad -> majChord
        P.MinorTriad -> minChord
        P.DiminishedTriad -> dimChord
        P.AugmentedTriad -> augChord
        P.DominantSeventh -> dom7Chord
        P.MajorSeventh -> maj7Chord
        P.MinorMajorSeventh -> minmaj7Chord
        P.MinorSeventh -> min7Chord
        P.AugmentedMajorSeventh -> augmaj7Chord
        P.AugmentedSeventh -> aug7Chord
        P.HalfDiminishedSeventh -> halfdim7Chord
        P.DiminishedSeventh -> dim7Chord
        _ -> error "Suspended chords are not implemented yet"

repl :: ReplState -> Music ()
repl st = do
   line <- prompt
   case replMode st of
     NoteMode ->
       case parse ((P.pCommandOr P.pSeq) <* eof) "<stdin>" line of
         Right (Left cmd) -> handleCommand cmd
         Right (Right seq) -> do
           liftIO $ putStrLn $ show seq
           play $ convertSeq seq
           repl st
         Left err -> do
           liftIO $ putStrLn $ errorBundlePretty err
           repl st
     ChordMode ->
       case parse ((P.pCommandOr P.pChords <* eof)) "<stdin>" line of
         Right (Left cmd) -> handleCommand cmd
         Right (Right pcs) -> do
           seq <-
             mapM (\pc -> do
                    liftIO $ putStrLn $ show pc
                    case convertChord pc of
                      Right c -> do
                        liftIO $ putStrLn $ intercalate ";" $ map getName $ notes c
                        return $ notes c
                      Left err -> do
                        liftIO $ putStrLn err
                        return [])
                 pcs
           play $ Seq seq
           repl st
           -- play $ convertSeq ns
         Left err -> do
           liftIO $ putStrLn $ errorBundlePretty err
           repl st
  where
    handleCommand (P.CBpm v) = do
      mst <- get
      put $ mst { bpm = v }
      repl st
    handleCommand P.CNotes = repl $ st { replMode = NoteMode }
    handleCommand P.CChords = repl $ st { replMode = ChordMode }
    prompt = do
      mst <- get
      let prefix = (show $ bpm mst) ++ "bpm "
      let p =
            case replMode st of
              NoteMode -> prefix ++ "notes> "
              ChordMode -> prefix ++ "chords> "
              KeyMode  -> prefix ++ "> "
      liftIO $ do
        putStr p
        hFlush stdout
        TIO.getLine

main :: IO ()
main = do
  mPiano <- defaultInstrument 1
  case mPiano of
    Just piano -> runMusic piano 140 $ repl $ ReplState NoteMode
      -- runMusic piano 140 $ song1
    Nothing -> do
      putStrLn "No available MIDI-output devices"
      exitSuccess
